# Learning objectives

!!! summary "Learning goals"

    After following this course you will be able:

    - to give a description of the basic quantum mechanical postulates and statements.
    - to interpret the wave function and apply operators to it to obtain information about a particle's physical properties such as position, momentum and energy.
    - to solve the Schroedinger equation to obtain wave functions for some basic types of potential in one dimension. 
    - to obtain the eigenfunctions and the corresponding eigenvalues for the harmonic oscillator using the Schrodinger equation.
    - to recognise symmetries and be able to take advantage of them in order to choose the appropriate method for solving problem.
    - to sketch the shape of the wavefunction based on the shape of the potential.
    - to apply the commutation relations of operators to determine whether or not two physical properties can be simultaneously measured.
    - to calculate probabilities of measurement outcomes.
    - to calculate expectation values.
    - to solve the energy eigenvalue problem.

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/sconesaboj/qm1-notes), especially do [let us know](https://gitlab.kwant-project.org/sconesaboj/qm1-notes/issues/new?issuable_template=typo) if you see a typo!
