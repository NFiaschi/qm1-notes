---
title: Determinate States
---

**Week 4, lecture 11**

# Determinate states and Hermitian operators

We are now going to discuss an important type of quantum states known as **determinate states**, which are those states for which the measurement of a given physical observables returns _always_ the same outcome. These states play an important role since, as we will shown, the eigenvectors of an Hermitian operator associated to a physical observable correspond to the determinate states of this same observable.

To start the discussion, assume that we have an ensemble of quantum systems, all of them prepared in the same **identical** quantum state characterized by the state vector $|\psi \rangle$. If now we measure the value of a given physical observable $Q$ in each of these identical systems, can we expect to always find the same result? 

The question might seem surprising, since in classical physics I will always obtain the same outcome if I repeat a given measurement in identical systems (neglecting the finite precision of the measurement). However, this is _not_ true in quantum physics: even if the quantum states $\psi \rangle$ that form the ensemble are identical, in general I will find **different results** each time I measure the observable $Q$ in one of these states. 

!!! example
    We have a spin $\frac{1}{2}$ system characterized by $|\psi \rangle = a |\uparrow \rangle + b |\downarrow \rangle$ and we let's measure its spin in the z-direction. Sometimes we will measure $+\frac{\hbar}{2}$ and for some other system we will measure $-\frac{\hbar}{2}$. 

However, there are some special states where a measurement of $Q$ always returns the same result. So, we define **determinate states** for the observable $Q$ as those in which if we measure many times we find alwasy the same result.

!!! example
    If $\hat{Q}=\hat{H}$, and if we have an ensemble of harmonic oscillators, all of them in the ground state, each time we measure the energy we will always find the same result, $E_0=\frac{\hbar \omega}{2}$. So, in this case the ground state is a determinate state.

!!! example
    Now if the $\hat{Q}=\hat{S_z}$ and our quantum state is $|\psi \rangle = |\uparrow \rangle$ then this state corresponds to a determinate state of the $S_z$ since every measurement returns $+{\hbar}{2}$

In terms of operators, a determinate state obeys and eigenvalue equation: $\hat{Q}|\psi \rangle = q |\psi \rangle$. Let's illustrate this concept with an example.

!!! example
    Let's consider a quantum system composed by only two energy levels: 

    $|\psi_1\rangle$ $\rightarrow$ $E_1$ and $|\psi_2\rangle$ $\rightarrow$ $E_2$ 

    By definition, we know that $|\psi_1\rangle$ and $|\psi_2\rangle$ are determinate states of the total energy E, that is they are eigenstates of the Hamiltonian $\hat{H}$:

    $$\hat{H}|\psi_1\rangle = E_1|\psi_1\rangle$$
    $$\hat{H}|\psi_2\rangle = E_2|\psi_2\rangle$$

    One can also construct valid quantum states which are not determinate, for instance: $\psi \rangle =\frac{1}{\sqrt{2}}|\psi_1\rangle-\frac{1}{\sqrt{2}}|\psi_2\rangle$. If we apply the $\hat{H}$ to this quantum state we find
    $$\hat{H}|\psi \rangle=\hat{H}(\frac{1}{\sqrt{2}}|\psi_1\rangle-\frac{1}{\sqrt{2}}\psi_2\rangle) = \frac{E_1}{\sqrt{2}}|\psi_1\rangle-\frac{E_2}{\sqrt{2}}|\psi_2\rangle \neq E |\psi \rangle$$. So, $|\psi \rangle$ is not a determinate state.

Now we know that determinate states are eigenstates of the Hermitian operator.

In the following we will see a number of important properties of the eigenvalues and eigenfunctions of the Hermitian operators.

Let's consider an eigenvalue equation for an Hermitian operator: 

$$\hat{Q}|\psi \rangle = q |\psi \rangle$$

As we have seen so far, the spectrum could be discrete (i.e. harmonic oscillator), continuous (i.e. the free particle), or a combination of a discrete and continuos (i.e. the delta fucntion and the finite potential well). 

Let's discuss some relevant properties for the discrete and continuous cases:

For the discrete case:

1. The eigenvalues are real ($q \in \mathbb{R}$).

    >*Proof*: Let's use the definition of an Hermitian operator, $\hat{Q}=\hat{Q}^{\dagger}$.
    $$\langle \psi |\hat{Q}|\psi \rangle = \langle \psi |\hat{Q} \psi \rangle = q \langle \psi | \psi \rangle$$
    $$\langle \psi |\hat{Q}|\psi \rangle = \langle \psi \hat{Q}| \psi \rangle = q^* \langle \psi | \psi \rangle$$
    $$ q \langle \psi | \psi \rangle = q^* \langle \psi | \psi \rangle$$
    therefore if a number is equal to its complex conjugate the number $q \in \mathbb{R}$.

2. The eigenfunctions associated to different eigenvalues are **orthogonal** among them:

    $$\int_{-\infty}^{+\infty}\psi^*_n \psi_l=\delta_{nl}$$

    >*Proof:* Let's consider two eigenfunctions of $\hat{Q}$ with different eigenvalues:
    $$\hat{Q}|\psi_1 \rangle = q_1 |\psi_1 \rangle$$
    and
    $$\hat{Q}|\psi_2 \rangle = q_2 |\psi_2 \rangle$$
    Since $\hat{Q}$ is Hermitian operator we have that, $\langle \psi_1 |\hat{Q}\psi_2 \rangle=\langle \psi_1 \hat{Q}|\psi_2 \rangle$, and this implies,
    $$q_2\langle \psi_1 |\psi_2 \rangle=q_1^*\langle \psi_1|\psi_2 \rangle$$
    But since we know that $q_1$ and $q_2$ are real, then the only way this equality is satistfied is if $\langle \psi_1|\psi_2 \rangle=0$
    Therefore, $|\psi_1 \rangle$ and $|\psi_2 \rangle$ are orthogonal.

3. The eigenstates of the operator $\hat{Q}$ associated to the observable $Q$ are **complete**. That is, any function in Hilbert space can be expressed as a linear combination of these eigenvalues: $\hat{Q}|\psi_n \rangle = q_n |\psi_n \rangle$, $|\Psi \rangle = \sum_n c_n |\psi_n \rangle$


For the continuous case:

!!! important
    In the continuous case the proofs of the properties of the discrete case do not apply, since the eigenstates are not normalizable and the inner product might not exist. So, we need a different way to show the same properties:
    1.The eigenvalues are real.
    2.The eigenstates are orthogonal.
    3.The eigenvalues form a complete basis.

We will show properties 1, 2, and 3 via examples.

!!! example "The eigenvales are real and orthogonal:"

    Say that we want to find the eigenvalues and eigenstates of the momentum operator $\hat{p}$. In the following we will work in the position representation, where $f_p(x)$ is the eigenfunction of the linear momentum operator.

    $$\hat{p}f_p(x)=pf_p(x)$$

    in this case the eigenvalue equation that we need to solve is:

    $$\frac{\hbar}{i}\frac{\partial}{\partial x} f_p(x)=p f_p(x)$$

    the general solution is: 

    $$f_p(x)=Ae^{i\frac{px}{\hbar}}$$

    which is not normalizable.

    Despite $f_p(x)$ is not normalizable, let's try to calculate the inner product of two different eigenfunctions: $f_{p_1}(x)$ and $f_{p_2}(x)$.

    $$\langle f_p_1(x)|f_p_2(x)\rangle = \int_{-\infty}^{+\infty} dx |A|^2 e^{i\frac{(p_1-p_2)x}{\hbar}}=$$
    $$|A|^2\int_{-\infty}^{+\infty} dx e^{i\frac{(p_2-p_1)x }{\hbar}}=$$

    we assume that the eigenvalues $p_1$ and $p_2$ are real (we are not going to demonstrate it)

    $$ =2\pi \hbar |A|^2 \delta (p_2-p_1)$$

    We chose $A=\frac{1}{\sqrt{2 \pi \hbar}}$ and we find that the eigenfunctions of the momentum operator $\hat{p}$ are,

    $$f_p(x)=\frac{1}{\sqrt{2 \pi \hbar}}e^{i\frac{px}{\hbar}}$$ and satifsy the following relation,

    $$\langle f_{p_1}(x)|f_{p_2}(x) \rangle =\delta (p_2-p_1)$$ or 

    $$\langle f_{p_1}(x)|f_{p_2}(x) \rangle =\delta (p_1-p_2)$$ because the Delta function is symmetric.

    If $p_1 \neq p_2$ then $\delta (p_2-p_1)=0$, which is teh analogue in the continuum case than the orthogonality relation in the discrete case.

    Note that, this relation $\langle f_{p_1}(x)|f_{p_2}(x) \rangle =\delta (p_2-p_1)$ is called **Dirac orthogonality**.

!!! example "The eigenfunctions form a complete basis:"

    $\psi(x)=\int_{-\infty}^{+\infty} dp c(p) f_p(x)=\frac{1}{\sqrt{2}}\int_{-\infty}^{+\infty} dp c(p) e^{i\frac{px}{\hbar}}$







